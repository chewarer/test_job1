# Тестовое задание №1. Приложение на Flask #

Добавление, удаление редактирование пользователей и их прав.

Авторизация сделана двумя способами: HTTP auth и через POST форму.

## Управление миграциями ##

По умолчанию в config.py подключена база SQLite. Там же можно переключить на БД Postgres.

После установки следует запустить скрипт применения миграции:

python run.py db migrate


Создание миграций с нуля:
* python run.py db init
* python run.py db migrate
* python run.py db upgrade


Скрипт запуска сервера:

python run.py runserver


## Описание API ##

* Login: /api/users/login (GET, POST)
* Самостоятельная регистрация: /api/users/signin (POST)
* Разлогинивание: /api/users/logout (GET)
* Получение списка пользователей: /api/users (GET)
* Добавление пользователя: /api/users (POST)
* Редактирование пользователя: /api/users/<int:user_id> (PUT)
* Удаление пользователя: /api/users/<int:user_id> (DELETE)