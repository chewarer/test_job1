import json
from functools import wraps

from werkzeug.http import parse_authorization_header
from flask import jsonify, make_response, request, g, render_template, redirect, url_for
from flask_login import current_user, login_user, login_required, logout_user

from app import app, db, lm
from app.models import User


def admin_required(f):
    """Декоратор. Проверка пользовательских прав"""
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if g.user.is_anonymous:
            return make_response(jsonify({'error': 'Required authorization'}), 401)
        elif g.user.role != User.ROLES['admin'] and not g.user.is_superuser:
            return make_response(jsonify({'error': 'Not allowed permissions'}), 403)
        return f(*args, **kwargs)
    return decorated_function


def anonymus_required(f):
    """Декоратор. Проверка залогинен ли пользователь"""
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if not g.user.is_anonymous:
            return make_response(jsonify({'error': 'User is already authorized'}), 400)
        return f(*args, **kwargs)
    return decorated_function


@lm.user_loader
def load_user(uid):
    return User.query.get(int(uid))


@app.before_request
def before_request():
    g.user = current_user
    print(g.user)


def get_post_data():
    """Получает данные из POST-запроса"""
    data = None
    if len(request.data):
        data = json.loads(request.data.decode('utf-8'))
    elif len(request.form):
        data = request.form
    return data


# Страница авторизации
@app.route('/log-in', methods=['GET'])
@anonymus_required
def login():
    return render_template('login.html')


# Страница выхода
@app.route('/log-out', methods=['GET'])
# @anonym_required
def logout_page():
    logout_user()
    return redirect(url_for('login'))


# Страница списка пользователей
@app.route('/', methods=['GET'])
@app.route('/users', methods=['GET'])
@login_required
def users_page():
    return render_template('users.html')


# Авторизация
@app.route('/api/users/login', methods=['GET', 'POST'])
@anonymus_required
def log_in():

    def basic_auth():
        """Авторизация  с помощью Basic HTTP-Auth"""
        print(request.headers)
        val = request.headers.get('Authorization')
        val = parse_authorization_header(val)
        if not val:
            response = make_response(jsonify({'error': 'Not found authorization header'}), 401)
            response.headers['WWW-Authenticate'] = 'Basic'
            return response
        return val

    if request.headers.get('Authorization') is not None:
        data = basic_auth()  # Авторизация  с помощью Basic HTTP-Auth
    else:
        data = get_post_data()  # Авторизация через форму
        if not data:
            return make_response(jsonify({'error': 'Not data'}), 400)

    username = data.get('username')
    password = data.get('password')
    user = User.query.filter_by(name=username, password=password).first()

    if user is not None:
        login_user(user, remember=True)
        return make_response(jsonify({'result': True}), 200)

    return make_response(jsonify({'result': False, 'error': 'Incorrect name or password'}), 400)


# Регистрация пользователем
@app.route('/api/users/signin', methods=['POST'])
@anonymus_required
def signin():
    data = get_post_data()  # Регистрация через форму
    if not data:
        return make_response(jsonify({'error': 'Not data'}), 400)
    username = data.get('username')
    password = data.get('password')

    if not username or not password:
        return make_response(jsonify({'error': 'Not enought data'}), 400)

    user = User.query.filter_by(name=username).first()
    if user:
        return make_response(jsonify({'error': 'This name is used'}), 400)

    user = User(name=username, password=password)
    # Если это первый пользователь, наделяем его правами суперюзера.
    if User.query.count() == 0:
        user.is_superuser = True
    db.session.add(user)
    db.session.commit()
    login_user(user, remember=True)

    return make_response(jsonify({'result': True}), 201)


# Logout
@app.route('/api/users/logout', methods=['GET'])
def logout():
    logout_user()
    return make_response(jsonify({'result': True}), 204)


# Список пользователей
@app.route('/api/users', methods=['GET'])
@login_required
def user_list():
    users = User.query.order_by(User.id.asc()).all()
    data = {'users': list()}
    for u in users:
        li = {
            'uid': u.id,
            'name': u.name,
            'role': u.get_role(u.role)
        }
        data['users'].append(li)
    try:  # На случай отключения декоратора login_required
        if g.user.is_superuser or g.user.role == User.ROLES['admin']:
            data['is_admin'] = True
    except AttributeError:
        pass
    return jsonify(data)


# Добавление пользователя
@app.route('/api/users', methods=['POST'])
@admin_required
def user_add():
    data = get_post_data()  # Получаем данные из POST-запроса
    if not data:
        return make_response(jsonify({'error': 'Not data'}), 400)
    username = data.get('username')
    password = data.get('password')
    role = data.get('role')

    if username and password:
        if not role:
            return make_response(jsonify({'error': 'Role required'}), 400)
        role = User.ROLES.get(role.lower())
        if role is None:
            return make_response(jsonify({'error': 'Role not exist'}), 400)
        # Break if user exists
        if User.query.filter_by(name=username).scalar():
            return make_response(jsonify({'error': 'User exist'}), 400)
        user = User(name=username, password=password, role=role)
        # Если это первый пользователь, наделяем его правами суперюзера.
        if User.query.count() == 0:
            user.is_superuser = True
        db.session.add(user)
        db.session.commit()
        return make_response(jsonify({'result': True}), 201)
    return make_response(jsonify({'error': 'Not enought data'}), 400)


# Редактирование пользователя
@app.route('/api/users/<int:user_id>', methods=['PUT'])
@admin_required
def user_edit(user_id):
    data = get_post_data()  # Получаем данные из POST-запроса
    if not data:
        return make_response(jsonify({'error': 'Not data'}), 400)
    username = data.get('username')
    role = data.get('role')

    if username and role:
        role = User.ROLES.get(role.lower())
        if role is None:
            return make_response(jsonify({'error': 'Role not found'}), 404)

        user = User.query.get(int(user_id))
        if not user:
            return make_response(jsonify({'error': 'User not found'}), 404)

        user.name = username
        user.role = role
        db.session.add(user)
        db.session.commit()
        return make_response(jsonify({'result': True}), 200)

    return make_response(jsonify({'error': 'Incorret request'}), 400)


# Удаление пользователя
@app.route('/api/users/<int:user_id>', methods=['DELETE'])
@admin_required
def delete_user(user_id):
    user = User.query.get(int(user_id))
    if not user:
        return make_response(jsonify({'error': 'Not found'}), 404)
    if user.is_superuser:
        return make_response(jsonify({'error': "Can't delete the superuser", 'result': False}), 403)

    db.session.delete(user)
    db.session.commit()
    return jsonify({'result': True})
