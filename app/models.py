from app import db


class User(db.Model):
    """Модель пользователя"""
    # Сделал права по-простому. Для более сложной работы с правами стоит сделать отдельной моделью.
    ROLES = {
        'user': 0,
        'admin': 1,
    }

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(16), unique=True)
    password = db.Column(db.String(256), nullable=False)  # Для простоты храню пароль как есть
    role = db.Column(db.SmallInteger, default=ROLES['user'])
    is_superuser = db.Column(db.Boolean, default=False, nullable=True)

    @property
    def is_authenticated(self):
        return True

    @property
    def is_active(self):
        return True

    @property
    def is_anonymous(self):
        return False

    def get_id(self):
        return str(self.id)

    def get_role(self, val):
        def get_key(d, value):
            for k, v in d.items():
                if v == value:
                    return k
        return get_key(self.ROLES, val)

    @property
    def is_admin(self):
        if self.role == User.ROLES['admin']:
            return True
        return False

    def __repr__(self):
        return '<User %r>' % self.name
