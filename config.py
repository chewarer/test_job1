CSRF_ENABLED = True
SECRET_KEY = 'GHGfhryfdhgd99s0797d%^%$dfgfgdfW4^%^%'
DEBUG = True

import os
basedir = os.path.abspath(os.path.dirname(__file__))

SQLALCHEMY_TRACK_MODIFICATIONS = True

POSTGRES = {
    'user': 'psyh',
    'password': '785236',
    'db': 'flask1',
    'host': '',
    'port': '5432',
}

# Postgres DB
# SQLALCHEMY_DATABASE_URI = 'postgresql://%(user)s: %(password)s@%(host)s:%(port)s/%(db)s' % POSTGRES

# SQLite DB
SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(basedir, 'app.db')

SQLALCHEMY_MIGRATE_REPO = os.path.join(basedir, 'db_repository')
